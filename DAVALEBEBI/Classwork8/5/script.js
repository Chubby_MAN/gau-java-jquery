function start_game(){
    k = setInterval(add_ball, 1500)
}

function stop_game(){
    clearInterval(k)
}



function add_ball(){
    var ball = document.createElement("div")
    ball.classList.add("kvadrati")
    var square = document.querySelector(".square")
    var width = parseInt(square.offsetWidth)-50
    ball.style.backgroundColor = "green"
    var top = Math.floor(Math.random()*350)
    var left = Math.floor(Math.random()*width)
    ball.style.top = top+"px"
    ball.style.left = left+"px"
    var ball_count = document.querySelector("#kvadrati-count")
    ball.addEventListener("click", removeBall)
    square.appendChild(ball)
    ball_count.innerText = parseInt(ball_count.innerText)+1
    if(parseInt(ball_count.innerText)==10){
        ball.removeEventListener("click", removeBall, true)
        clearInterval(k)
    }
    function removeBall(){
            var  remove_ball_count = document.getElementById("remove-ball-count")
            remove_ball_count.innerText = parseInt(remove_ball_count.innerText)+1;
            ball_count.innerText = parseInt(ball_count.innerText)-1
            this.parentElement.removeChild(this)
    }
}


