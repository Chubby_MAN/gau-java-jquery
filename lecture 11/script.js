function create_element(el){
    var button = document.createElement("button");
    var text = document.createTextNode("hello world")
    button.appendChild(text)
    button.style.background = "green"
    result = document.querySelector("#result")
    result.appendChild(button)
    console.log(button)
    button.addEventListener("click", function(){
        console.log(this)
        this.style.height = "5em"
    })

    var p = document.createElement("p")
    p.innerText = "javascript"
    result2 = document.querySelector("#result2")
    var p2 = result2.querySelector("p")[1]
    result2.insertBefore(p,p2)
    console.log(result2)
    console.log(el)
    el.setAttribute("disabled", "disabled")

    var parent = el.parentElement
    // parent.removeChild(el)
    setTimeout(function(){
        console.log(el)
        var parent = el.parentElement
        parent.removeChild(el)
    }, 3000)
}